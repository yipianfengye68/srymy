package com.srymy.gb28181.part1.sip;

import javax.sip.InvalidArgumentException;
import javax.sip.RequestEvent;
import javax.sip.ResponseEvent;
import javax.sip.SipProvider;
import javax.sip.address.AddressFactory;
import javax.sip.header.HeaderFactory;
import javax.sip.message.MessageFactory;

/**
 * @Author: cth
 * @Date: 2019/10/22 13:37
 * @Description:
 */
public interface SipSrymyMessageProcessor {

    /**
     * 接收IPCamera发来的SIP协议消息的时候产生的回调函数
     */
    public void processRequest(RequestEvent requestEvent, AddressFactory addressFactory, MessageFactory messageFactory, HeaderFactory headerFactory, SipProvider sipProvider);

    /**
     * 接收IPCamera发来的SIP协议消息的时候产生的回调函数
     */
    public void processResponse(ResponseEvent responseEvent, AddressFactory addressFactory, MessageFactory messageFactory, HeaderFactory headerFactory, SipProvider sipProvider) throws InvalidArgumentException;

    public void processError(String errorMessage);
}
